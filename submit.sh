#!/bin/sh
### Job name
#PBS -N helloMPI
### Output files
#PBS -o hello.out
#PBS -e hello.err
### Specify Resrouces
#PBS -l nodes=1:ppn=1

source /share/apps/Modules/3.2.10/init/sh

# move into direcotry
cd ~/tutorial

# Octave example
module load intel
module load octave
octave example.m


# Python Example
# module load python/2.7.8
# python example.py

# Sleep to let us see jobs
sleep 10
