HPCTutorial
---
Just a quick overview of how to connect to the HPCs

What are HPCS?
==
HPC stands for High Powered Computers. This system allows use to run programs in a distributes and parallel manner. The University of South Carolina provides HPCS as a reasource for free. This is an overview of how to run basic programs with HPCs.

A great place to learn more about these resources is directly from the University Website: [http://rci.cec.sc.edu/wordpress/](http://rci.cec.sc.edu/wordpress/)

Login
==
Once set up with an account, ssh into the Maxwell head Node on port 222 (`ssh -P222 name@maxwell.cec.sc.edu`) and use the Duo app to authenticate.

Running a Job
==
Once your code is set up, create a shell script similar to `submit.sh`. Use the `#PBS` notation to express Torque scheduler parameters.

  - Submit a job `qsub submit`
  - Check in on job `qsub -u name`

For more check out the [HPC website tutorial section](http://zion.cec.sc.edu/wordpress/pbs-queue/)

The file system is shared between nodes, so every running node can access written files. For true parallel computing, checkout the [MPI library](http://www.mcs.anl.gov/research/projects/mpi/learning.html). This set up elects a master and delegates tasks to other nodes. Other means 